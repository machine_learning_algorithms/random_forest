import numpy as np
import pandas as pd
from random import seed
from random import randrange
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import BaggingClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split, KFold
import matplotlib.pyplot as plt


# read in data
df = pd.read_csv('../data/cleveland_heart.csv',na_values=['?'])
# df.replace('?',np.NaN)
df = df.dropna()

# bag data 
tree = DecisionTreeClassifier()
bag = BaggingClassifier(tree,n_estimators=100,max_samples=0.5,random_state=1)

X = df[df.columns[:-1]]
y = df.num
bag.fit(X,y)

# scikit learn models
skmodel = RandomForestClassifier(n_estimators=100,random_state=0)

# Calculate the mean square error a split dataset
def mse(groups,y):
    # measure mean square error of split
    # weight entropy by group size
    totalSamples= float(sum([len(group) for group in groups]))
    # sum weighted Gini index for each group
    mse = 0.
    for group in groups: # RF does binary split
        groupSize = float(group.shape[0])
        # avoid divide by zero
        if groupSize == 0:
            continue
        # calculate mse for the group
        square_err = np.sum( (group[:,-1]-y)**2 ) / groupSize # mse for group
    mse += square_err # total mse
    return mse


# Calculate the entropy (information gain) for a split dataset
def entropy(groups,classes):
    # measure entropy of split
    # H = - sum{i} p[i]*log(p[i])
    # weight entropy by group size
    totalSamples= float(sum([len(group) for group in groups]))
    # sum weighted Gini index for each group
    entropy = 0.
    for group in groups: # RF does binary split
        groupSize = float(group.shape[0])
        # avoid divide by zero
        if groupSize == 0:
            continue
        # score the group based on entropy
        for classVal in classes:
            p = (group[:,-1]==classVal).sum() / groupSize # numInClass/size(group) 
            entropyClass += -p*np.log2(p) # entropy for this class of values
        # weight the group entropy by its relative size
        entropy += entropyClass * (groupSize / totalSamples)
    return entropy 



# Calculate the Gini index for a split dataset
def gini_index(groups,classes):
    # measure how often random element from group is incorrectly labeled 
    # Gini = 1 - sum{1:numClasses} p^2
    # weight purity by group size
    totalSamples= float(sum([len(group) for group in groups]))
    # sum weighted Gini index for each group
    gini = 0.
    for group in groups: # only L/R groups
        groupSize = float(group.shape[0])
        # avoid divide by zero
        if groupSize == 0:
            continue
        score = 0.
        psquared = 0.
        # score the group based on class scores
        for classVal in classes:
            p = (group[:,-1]==classVal).sum() / groupSize # numInClass/size(group) 
            psquared += p**2 # measure of group purity wrt classes
        # weight the group score by its relative size
        gini += (1.0 - psquared) * (groupSize / totalSamples)
    return gini

# Split data based on feature and its value
def try_split(try_idx,fxVal,X,y):
    rowLen = X.shape[1]
    leftX, rightX = np.empty([1,rowLen]),np.empty([1,rowLen])
    lefty, righty = np.empty([1,1]),np.empty([1,1])
    for row in X:
        for idx,row in enumerate(X):
            if row[try_idx] < fxVal:
                leftX = np.vstack((leftX,row))
                lefty = np.vstack((lefty,y[idx]))
            else:
                rightX = np.vstack((rightX,row))
                righty = np.vstack((righty,y[idx]))
    lgroup = np.hstack((leftX,lefty))
    rgroup = np.hstack((rightX,righty))
    return lgroup[1:,:], rgroup[1:,:] # remove initialization row
 
# calculating splits
def get_split(X,y,numFeatures):
    # takes numpy arrays for X,y
    classVals = np.unique(y)
    features = []
    b_score = np.inf
    while len(features) < numFeatures:
        idx = randrange(X.shape[1]) # random feature selection 0-idx
        if idx not in features:
            features.append(idx)
    for idx in features:
        for row in X: # for one sample pt
            groups = try_split(idx,row[idx],X,y) # left, right groups
            gini = gini_index(groups,classVals)
            if gini < b_score: # iterate to find lowest b score; for regression use mse
                b_idx,b_val,b_score,b_lrgrps = idx,row[idx],gini,groups
    return {'index':b_idx,'value':b_val,'groups':b_lrgrps} # idx is feature

# create terminal node value
def to_terminal(group):
    outcomes = [row[-1] for row in group]
    return max(set(outcomes), key=outcomes.count) # mode/mean of set

# create child splits for a node or make terminal
def split(node, max_depth, min_size, n_features, depth):
    left, right = node['groups']
    del(node['groups'])
    # check for a no split
    if left.size==0 or right.size==0:
        node['left'] = node['right'] = to_terminal(left+right)
        return
    # check for max depth 
    if depth >= max_depth:
        node['left'],node['right'] = to_terminal(left),to_terminal(right)
        return
    # process left child 
    if len(left) <= min_size:
        node['left'] = to_terminal(left)
    else:
        node['left'] = get_split(left[:,:-1],left[:,-1],n_features)
        split(node['left'],max_depth,min_size,n_features,depth+1)
    # process right child 
    if len(right) <= min_size:
        node['right'] = to_terminal(right)
    else:
        node['right'] = get_split(right[:,:-1],right[:,-1],n_features)

        split(node['right'],max_depth,min_size,n_features,depth+1)

# Build a decision tree
def build_tree(trainX,trainY,maxDepth,minSize,numFeatures):
    trainX = trainX.values
    trainY = trainY.values
    root = get_split(trainX,trainY,numFeatures)
    split(root,maxDepth,minSize,numFeatures, 1)
    return root
 
# predict with list of bagged trees
def bagging_predict(trees,row):
    predictions = [predict(tree,row) for tree in trees]
    return max(set(predictions), key=predictions.count) 

# predict with a decision tree on single sample
def predict(node, row):  
    if row[node['index']] < node['value']:
        if isinstance(node['left'], dict):
            return predict(node['left'], row)
        else:
            return node['left']
    else:
        if isinstance(node['right'], dict):
            return predict(node['right'], row)
        else:
            return node['right']
 
# Random Forest Algorithm
def random_forest(train,test,max_depth,min_size,sample_size,n_trees,n_features):
    trees = []
    for i in range(n_trees):
        samples = train.sample(frac=sample_size,replace=False)
        trainX, trainY = samples.iloc[:,:-1], samples.iloc[:,-1]
        tree = build_tree(trainX,trainY,max_depth,min_size,n_features)
        trees.append(tree)
    predictions = [bagging_predict(trees,row) for row in test]
    return(predictions)

# Evaluate an algorithm using a cross validation for k-folds 
def crossvalidation(X,y,numFolds,*args):
    kf = KFold(n_splits = numFolds)
    fold_scores = []
    for trainIdx, valIdx in kf.split(X):
        predicted = random_forest(X.iloc[trainIdx],X.iloc[valIdx],*args) 
        acurracy = (predicted == y[valIdx]).sum() * 100.0 
        scores.append(accuracy)
    return scores


# main
seed(1234)
# load data
filename = 'random_forest.txt'
dataset = pd.read_csv('../data/cleveland_heart.csv',na_values=['?'])
# df.replace('?',np.NaN)
df = dataset.dropna()
# split into train/val/test sets
train, test = train_test_split(df,test_size=0.2,random_state=1234)
train, val = train_test_split(train, test_size = 0.25,random_state = 8888)
# train
trainX = train.loc[:,train.columns!='num']
trainY = train['num']

# evaluate random forest
n_folds = 5
max_depth = 10
min_size = 1
sample_size = 0.5
sqFeatures = int(np.sqrt(dataset.shape[1]-1)) # sqrt of features
# train classifier predict on out-of-bag data
for n_trees in [1,5,10]:
    scores = crossvalidation(trainX,trainY,n_folds,max_depth,min_size,sample_size,n_trees,sqFeatures)
    print('Trees: %d' % n_trees)
    print('Scores: %s' % scores)
    print('Mean Accuracy: %.3f%%' % (sum(scores)/float(len(scores))))
    print('Test scores: ')
